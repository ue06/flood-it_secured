#ifndef __EXO4__
#define __EXO4__

#include "Liste_case.h"

static const int default_distance = 9999999;	/* magic number. Static: file scope. */

typedef struct sommet Sommet;

/* Element d'une liste chainee de pointeurs sur Sommets */
typedef struct cellule_som
{
	Sommet *sommet;
	struct cellule_som *suiv;
} Cellule_som; 

struct sommet
{
	int num;	/* Numero du sommet (sert uniquement a l'affichage) */
	int cl;		/* Couleur d'origine du sommet-zone */
	ListeCase cases;	/* Listes des cases du sommet-zone */
	int nbcase_som; /* Nombre de cases de cette liste */

	Cellule_som *sommet_adj;	/* Liste des aretes pointeurs sur les sommets adjacent */
	
	int marque;	/* 0 si dans Zsg, 1 si dans bordure, 2 si non visite */

	int distance;	/* Nombre d'arete reliant ce sommet a la racine du parcours en largeur */
	Sommet *pere;	/* Pere du sommet dans l'arborescence du parcours en largeur. Non-null ssi visite */
};

typedef struct graphe_zone
{
	int nbsom;	/* Nombre de sommets dans le graphe */
	Cellule_som *som;	/* Liste chainee des sommets du graphe */
	Sommet ***mat;	/* Matrice de pointeurs sur les sommets indiquant a quel sommet appartient une case (i,j) de la grille */
} Graphe_zone;

/* ajoute un pointeur sur Sommet à une liste chaı̂née de Cellule som passée
en paramètre */
void ajoute_liste_sommet(Sommet *psom, Cellule_som **liste);

/* détruit une liste chaı̂née de Cellule som SANS détruire les sommets
pointées par cette liste */
void detruit_liste_sommet(Cellule_som *liste);

/*  met à jour deux sommet s1 et s2 en indiquant qu’ils
sont adjacents l’un de l’autre */
void ajoute_voisin(Sommet *s1, Sommet *s2);

/* renvoie vrai (1) si et seulement si deux sommets sont adjacents, sinon renvoie 0 */
int adjacent(Sommet *s1, Sommet *s2);

/* creer le graphe tout entier */
Graphe_zone *cree_graphe_zone(int **M, int dim);

/* affichage du graphe: liste d'adjacence */
void affiche_graphe_zone(Graphe_zone *pg);

/* delete graphe zone */
void libere_graphe_zone(Graphe_zone *pg, int dim);

/* test run, memory leak not dealt with */
void test_run_exo_4(int **M, int dim);

#endif
