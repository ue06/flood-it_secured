#include <math.h>
#include "Exo4.h"
#include "Exo1.h"	/* to use trouve_zone_rec(...) */

/* ajoute en tete */
void ajoute_liste_sommet(Sommet *psom, Cellule_som **liste)
{
	Cellule_som *pc = (Cellule_som *)malloc(sizeof(Cellule_som));
	pc->sommet = psom;
	pc->suiv = *liste;

	*liste = pc;
}

void detruit_liste_sommet(Cellule_som *liste)
{
	if (liste == NULL) return;
	detruit_liste_sommet(liste->suiv);
	free(liste);
}

void ajoute_voisin(Sommet *s1, Sommet *s2)
{
	ajoute_liste_sommet(s2, &(s1->sommet_adj));
	ajoute_liste_sommet(s1, &(s2->sommet_adj));
};

int adjacent(Sommet *s1, Sommet *s2)
{
	int found;

	/* Can I find s2 in s1->sommet_adj ? */
	found = 0;
	for (Cellule_som *pc = s1->sommet_adj; pc != NULL; pc = pc->suiv) {
		if (s2 == pc->sommet) found = 1;
	}
	if (found == 0) return 0;

	/* Double check (optional): can I find s1 in s2->sommet_adj ? */
	found = 0;
	for (Cellule_som *pc = s2->sommet_adj; pc != NULL; pc = pc->suiv) {
		if (s1 == pc->sommet) found = 1;
	}
	if (found == 0) return 0;

	return found; // which is 1 now anyway
}

Graphe_zone *cree_graphe_zone(int **M, int dim)
{
	/* create new null-initialized graphe zone */
	Graphe_zone *pg = (Graphe_zone *)malloc(sizeof(Graphe_zone));
	pg->nbsom = 0;
	pg->som = NULL;
	pg->mat = (Sommet ***)malloc(sizeof(Sommet **) * dim);
	for (int i = 0; i < dim; i++) {
		pg->mat[i] = (Sommet **)malloc(sizeof(Sommet *) * dim);
		for (int j = 0; j < dim; j++) {
			pg->mat[i][j] = NULL;
		}
	}

	/* create its sommets */
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {

			/* find NULL */
			if (pg->mat[i][j] == NULL) {
				
				/* create new sommet */
				Sommet *ps = (Sommet *)malloc(sizeof(Sommet));
				ps->num = (++(pg->nbsom));
				ps->cl = M[i][j];
				ps->cases = NULL;
				ps->nbcase_som = 0;
				ps->sommet_adj = NULL;
				ps->marque = 2;
				ps->distance = default_distance;
				ps->pere = NULL;

				/* find this sommet's zone (find its tiles) */
				trouve_zone_rec(M, dim, i, j, &(ps->nbcase_som), &(ps->cases));

				/* put this newly created Sommet in graphe_zone->som */
				ajoute_liste_sommet(ps, &(pg->som));

				/* mettre ensuite a jour la matrice mat pour toutes les cases de la zone */
				for (Elnt_liste *pe = ps->cases; pe != NULL; pe = pe->suiv) {
					pg->mat[pe->i][pe->j] = ps;
				}
			}
		}
	}

	/* create its aretes */
	for (int i = 0; i < dim; i++) {
		for (int j = 0; j < dim; j++) {

			/* check up, down, left, right for adjacent tiles belonging to another sommet */
			int src_color = M[i][j];
			Sommet *src_s = pg->mat[i][j];

			// up
			if (j != 0) {
				int up_color = M[i][j-1];
				Sommet *up_s = pg->mat[i][j-1];
				// if different color
				if (up_color != src_color) {
					// if new adjacency info
					if (adjacent(src_s, up_s) == 0) {
						ajoute_voisin(src_s, up_s);
					}
				}
			}

			// down
			if (j != dim-1) {
				int down_color = M[i][j+1];
				Sommet *down_s = pg->mat[i][j+1];
				// if different color
				if (down_color != src_color) {
					// if new adjacency info
					if (adjacent(src_s, down_s) == 0) {
						ajoute_voisin(src_s, down_s);
					}
				}
			}

			// left
			if (i != 0) {
				int left_color = M[i-1][j];
				Sommet *left_s = pg->mat[i-1][j];
				// if different color
				if (left_color != src_color) {
					// if new adjacency info
					if (adjacent(src_s, left_s) == 0) {
						ajoute_voisin(src_s, left_s);
					}
				}
			}

			// right
			if (i != dim-1) {
				int right_color = M[i+1][j];
				Sommet *right_s = pg->mat[i+1][j];
				// if different color
				if (right_color != src_color) {
					// if new adjacency info
					if (adjacent(src_s, right_s) == 0) {
						ajoute_voisin(src_s, right_s);
					}
				}
			}
		}
	}

	/* it is done */
	return pg;
}

void affiche_graphe_zone(Graphe_zone *pg) {
	printf("Le graphe a %d sommets.\n", pg->nbsom);

	for (Cellule_som *pcs = pg->som; pcs != NULL; pcs = pcs->suiv) {
		Sommet *ps = pcs->sommet;
		printf("\nSommet numero %d de couleur %d et de taille %d a pour liste des sommets adjacents: [", ps->num, ps->cl, ps->nbcase_som);
		for (Cellule_som *pcs_a = ps->sommet_adj; pcs_a != NULL; pcs_a = pcs_a->suiv) {
			printf("%d", pcs_a->sommet->num);
			if (pcs_a->suiv != NULL) {
				printf(", ");
			}
		}
		printf("].");

		/* Additional fields added in exo 5 and 6 */
		printf("\nMarque %d. Distance %d.\n", ps->marque, ps->distance);
		if (ps->pere != NULL) {
			printf(" Pere %d.", ps->pere->num);
		}
	}
}

void libere_graphe_zone(Graphe_zone *pg, int dim)
{
	/* free som*/
	for (Cellule_som *pc = pg->som; pc != NULL; pc = pc->suiv) {
		Sommet *ps = pc->sommet;
		/* free cases */
		Elnt_liste *pe = ps->cases;
		Elnt_liste *tmppe = NULL;
		while (pe != NULL) {
			tmppe = pe->suiv;
			free(pe);
			pe = tmppe;
		}
		/* free sommet_adj */
		detruit_liste_sommet(ps->sommet_adj);
		free(ps);
	}

	/* free mat */
	for (int i = 0; i < dim; i++) {
		free(pg->mat[i]);
	}
	free(pg->mat);
	free(pg);
};

void test_run_exo_4(int **M, int dim) {
	Graphe_zone *pg = cree_graphe_zone(M, dim);
	affiche_graphe_zone(pg);
	libere_graphe_zone(pg, dim);
}