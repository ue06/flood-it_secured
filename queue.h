#ifndef __QUEUE__
#define __QUEUE__

#include "Exo4.h"

/* Element d'une liste doublement chainee des sommets */
typedef struct elnt_sommet{
	Sommet *sommet;
	struct elnt_sommet *prev;
	struct elnt_sommet *next;
} Elnt_sommet;

/* Une file. Implementation par une liste doublement chainee */
typedef struct queue{
	int length;		/* nombre d'elements dans la file */
	Elnt_sommet *start;
	Elnt_sommet *end;
} Queue;

/* Initialise une file vide */
Queue *creer_file();

/* Ajoute un element en tete de la file */
int empile(Queue *pq, Sommet *ps);

/* teste si une file est vide. 1 si vide. 0 si non vide. */
int test_file_vide(Queue *pq);

/* Depiler un element a la fin de la file */
Sommet *depile(Queue *pq);

/* Detruit tous les elements de la file */
void detruit_file(Queue *pq);

#endif