#ifndef __EXO6__
#define __EXO6__

#include "API_Grille.h"
#include "Liste_case.h"
#include "Exo5.h"
#include "queue.h"

void parcours_en_largeur(Sommet *start, Sommet *goal);

int strategie_inferieur_droite(int **M, Grille *G, int dim, int nbcl, int aff);

#endif