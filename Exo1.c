#include<stdio.h>
#include "Exo1.h"

void trouve_zone_rec(int **M, int nbcase, int i, int j, int *taille, ListeCase *L){
	int src_color = M[i][j];

	// add myself
	ajoute_en_tete(L, i, j);
	*taille = *taille + 1;

	/*
	 *	check tile up, down, left, right
	 *	if same color and not yet checked,
	 *	search from there as well
	 */

	// up
	if (j != 0) {
		int up_color = M[i][j-1];
		// if same color
		if (up_color == src_color) {
			// and not yet checked
			int checked = 0;
			for (Elnt_liste *pe = *L; pe != NULL; pe = pe->suiv) {
				if (pe->i == i && pe->j == j-1) checked = 1;
			}
			if (checked != 1) {
				trouve_zone_rec(M, nbcase, i, j-1, taille, L);
			}
		}
	}

	// down
	if (j != nbcase-1) {
		int down_color = M[i][j+1];
		// if same color
		if (down_color == src_color) {
			// and not yet checked
			int checked = 0;
			for (Elnt_liste *pe = *L; pe != NULL; pe = pe->suiv) {
				if (pe->i == i && pe->j == j+1) checked = 1;
			}
			if (checked != 1) {
				trouve_zone_rec(M, nbcase, i, j+1, taille, L);
			}
		}
	}

	// left
	if (i != 0) {
		int left_color = M[i-1][j];
		// if same color
		if (left_color == src_color) {
			// and not yet checked
			int checked = 0;
			for (Elnt_liste *pe = *L; pe != NULL; pe = pe->suiv) {
				if (pe->i == i-1 && pe->j == j) checked = 1;
			}
			if (checked != 1) {
				trouve_zone_rec(M, nbcase, i-1, j, taille, L);
			}
		}
	}

	// right
	if (i != nbcase-1) {
		int right_color = M[i+1][j];
		// if same color
		if (right_color == src_color) {
			// and not yet checked
			int checked = 0;
			for (Elnt_liste *pe = *L; pe != NULL; pe = pe->suiv) {
				if (pe->i == i+1 && pe->j == j) checked = 1;
			}
			if (checked != 1) {
				trouve_zone_rec(M, nbcase, i+1, j, taille, L);
			}
		}
	}
}


int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff){
  int cpt = 0;

  int new_color;
  // get random color
  do{
  	new_color = rand() % nbcl;
  } while(new_color == M[0][0]);

  // find zsg
  ListeCase zsg = NULL;
  int taille = 0;
  init_liste(&zsg);
  trouve_zone_rec(M, dim, 0, 0, &taille, &zsg);

  // have i won?
  if (taille == dim * dim) return 0;

  // modify color in zsg
  for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
  	M[pe->i][pe->j] = new_color;
  }

  // display
  if (aff == 1) {
  	// update color of Grille G and redraw
  	for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
  		Grille_attribue_couleur_case(G,pe->i,pe->j,new_color);
	}
	
	// note: if no display needed, Grille G wont be updated like matrix M
	Grille_redessine_Grille(G);
	Grille_attente_touche();
  }

  // destroy list zsg
  detruit_liste(&zsg);

  // recursive call
  cpt = 1 + sequence_aleatoire_rec(M, G, dim, nbcl, aff);

  return cpt;
}