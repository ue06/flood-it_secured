#include "Exo6.h"

void parcours_en_largeur(Sommet *start, Sommet *goal)
{
	Queue *pq = creer_file();
	
	/* initialize */
	empile(pq, start);
	start->distance = 0;
	// printf("Empile sommet numero %d.\n", start->num);

	while (test_file_vide(pq) == 0) {
		/* treat next sommet in line */
		Sommet *ps = depile(pq);
		// printf("Depile sommet numero %d.\n", ps->num);

		/* if this is our goal, stop */
		if (ps == goal) {
			break;
		}

		/* add unvisited children to queue */
		for (Cellule_som *pc = ps->sommet_adj; pc != NULL; pc = pc->suiv) {
			Sommet *child = pc->sommet;

			/* if visited (distance is changed), skip */
			if (child->distance != default_distance) {
				continue;
			}

			child->pere = ps;
			child->distance = ps->distance + 1;
			// printf("Empile sommet numero %d.\n", child->num);
			empile(pq, child);
		}
	}
};

int strategie_inferieur_droite(int **M, Grille *G, int dim, int nbcl, int aff)
{
	Graphe_zone *pg = cree_graphe_zone(M, dim);
	
	Sommet *start = pg->mat[0][0];	/* sommet superieur gauche */
	Sommet *goal = pg->mat[dim-1][dim-1];	/* sommet inferieur droite */
	
	parcours_en_largeur(start, goal);	/* writes traversal information in struct Sommet itself */

	// affiche_graphe_zone(pg);

	/* get color sequence */
	/* note: i need a stack to backtrace from ZID to ZSG. Too lazy though, will just use ListeCase->i. It is already a stack */

	ListeCase color_backtrace = NULL;
	init_liste(&color_backtrace);

	Sommet *tracer = goal;
	while (tracer != NULL) {
		ajoute_en_tete(&color_backtrace, tracer->cl, tracer->cl);
		tracer = tracer->pere;
	}

	/* now just depile color_backtrace for those colors */

	int count = 0;

	/* create zsg for display */
	ListeCase zsg = NULL;
	init_liste(&zsg);

	Bordure_graphe *pb = creer_bordure(nbcl, pg);

	while (test_liste_vide(&color_backtrace) == 0) {
		int color = -1;
		enleve_en_tete(&color_backtrace, &color, &color);

		/* update bordure following color_backtrace */
		maj_bordure(pb, &zsg, color);
		count++;

		// display
		if (aff == 1) {
			// update color of Grille G and redraw
			for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
				Grille_attribue_couleur_case(G,pe->i,pe->j,color);
			}
			// note: if no display needed, Grille G wont be updated like matrix M
			Grille_redessine_Grille(G);
			Grille_attente_touche();
		}
	}

	while (1) {
		/* stop if no remaining border tile */
		int sum = 0;
		for (int i = 0; i < nbcl; i++) {
			sum += pb->liste_tailles[i];
		}
		if (sum == 0) break;

		/* MAJ bordure */
		int color = maj_bordure(pb, &zsg, get_max_color(pb));
		count++;

		// display
		if (aff == 1) {
			// update color of Grille G and redraw
			for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
				Grille_attribue_couleur_case(G,pe->i,pe->j,color);
			}
			// note: if no display needed, Grille G wont be updated like matrix M
			Grille_redessine_Grille(G);
			Grille_attente_touche();
		}
	}

	return count;
};

