#ifndef __EXO5__
#define __EXO5__

#include "API_Grille.h"
#include "Liste_case.h"
#include "Exo4.h"

typedef struct bordure_graphe{
	int nbcl;	/* nombre de couleurs */
	Cellule_som **liste_sommets;	/* un tableau de nbcl listes chainees pour nbcl couleurs */
	int *liste_tailles;	/* taille de chaque liste correspondant aux couleurs representee dans la bordure */
} Bordure_graphe;

Bordure_graphe *creer_bordure(int nbcl, Graphe_zone *g);

/* choose most represented color */
int get_max_color(Bordure_graphe *pb);

/* met à jour la bordure-graphe en
basculant une couleur de la bordure dans la Zsg */
int maj_bordure(Bordure_graphe *pb, ListeCase *zsg, int color);

int strategie_max_bordure(int **M, Grille *G, int dim, int nbcl, int aff);

void libere_bordure(Bordure_graphe *pb);

#endif