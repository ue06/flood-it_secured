#! /bin/bash

aff=0
# notice we need the same random seed to compare algorithms
graine=$RANDOM

liste_exo="1 2 3 5 6"

########################################################################
echo "On fixe dim et diff et on varie >nbcl<"
########################################################################
dim=40
diff=100
liste_nbcl="5 10 15 20"

[ -f "f(nbcl)_tmp.txt" ] &&  rm -f "f(nbcl)_tmp.txt"
for nbcl in $liste_nbcl; do
	echo -ne "($dim,$diff,$nbcl)"
	for exo in $liste_exo; do
		result_it=0
		result_temps=0
		# test 10 times
		for i in {1..10}; do
			result=$(../Main_Flood-It ${dim} ${nbcl} ${diff} ${graine} ${exo} ${aff})
			result_it=$(($result_it + $(echo $result | cut -d' ' -f1)))
			result_temps=$(bc -l <<< "scale=6; $result_temps + $(echo $result | cut -d' ' -f2) ")
		done
		# calculer la moyenne
		result_it=$(bc -l <<< "scale=0; ${result_it}/10")
		result_temps=$(bc -l <<< "scale=6; ${result_temps}/10")
		echo -ne "\t${result_it}\t${result_temps}"
	done
	echo ""
done >> "f(nbcl)_tmp.txt"

[ -f "f(nbcl)_TIME.txt" ] &&  rm -f "f(nbcl)_TIME.txt"
[ -f "f(nbcl)_IT.txt" ] &&  rm -f "f(nbcl)_IT.txt"
# store It et Time separatly
while read line; do
	echo  $line | cut -d' ' -f'1,2,4,6,8,10,12' >> "f(nbcl)_IT.txt"
	echo  $line | cut -d' ' -f'1,3,5,7,9,11,13' >> "f(nbcl)_TIME.txt"
done < "f(nbcl)_tmp.txt" 
rm -f "f(nbcl)_tmp.txt"

# just messages
[ -f "f(nbcl).txt" ] &&  rm -f "f(nbcl).txt"
echo -e "f(nbcl)\ndim=$dim\ndiff=$diff\ngraine=$graine\n" >>"f(nbcl).txt"
echo "ITERATIONS:" >> "f(nbcl).txt"
echo -e "(dm,df,cl)\tex1\tex2\tex3\tex5\tex6"	>> "f(nbcl).txt"
cat "f(nbcl)_IT.txt" >> "f(nbcl).txt"
rm -f "f(nbcl)_IT.txt"  
echo -e "\nTIME:" >> "f(nbcl).txt"
cat "f(nbcl)_TIME.txt" >> "f(nbcl).txt"
# need to replace spaces by tabs to easy fit into Excel in order to draw courbes
sed -E -i 's: :\t:g' "f(nbcl).txt"

rm -f "f(nbcl)_TIME.txt"


########################################################################


########################################################################
echo "On fixe dim et nbcl et on varie >diff<"
########################################################################
dim=40
nbcl=15
liste_diff="1 10 20 50 100"

[ -f "f(diff)_tmp.txt" ] &&  rm -f "f(diff)_tmp.txt"
for diff in $liste_diff; do
	echo -ne "($dim,$diff,$nbcl)"
	for exo in $liste_exo; do
		result_it=0
		result_temps=0
		# test 10 times
		for i in {1..10}; do
			result=$(../Main_Flood-It ${dim} ${nbcl} ${diff} ${graine} ${exo} ${aff})
			result_it=$(($result_it + $(echo $result | cut -d' ' -f1)))
			result_temps=$(bc -l <<< "scale=6; $result_temps + $(echo $result | cut -d' ' -f2) ")
		done
		result_it=$(bc -l <<< "scale=0; ${result_it}/10")
		result_temps=$(bc -l <<< "scale=6; ${result_temps}/10")
		echo -ne "\t${result_it}\t${result_temps}"
	done
	echo ""
done >> "f(diff)_tmp.txt"

[ -f "f(diff)_TIME.txt" ] &&  rm -f "f(diff)_TIME.txt"
[ -f "f(diff)_IT.txt" ] &&  rm -f "f(diff)_IT.txt"
while read line; do
	echo  $line | cut -d' ' -f'1,2,4,6,8,10,12' >> "f(diff)_IT.txt"
	echo  $line | cut -d' ' -f'1,3,5,7,9,11,13' >> "f(diff)_TIME.txt"
done < "f(diff)_tmp.txt" 
rm -f "f(diff)_tmp.txt"

[ -f "f(diff).txt" ] &&  rm -f "f(diff).txt"
echo -e "f(diff)\ndim=$dim\nnbcl=$nbcl\ngraine=$graine\n" >>"f(diff).txt"

echo "ITERATIONS:" >> "f(diff).txt"
echo -e "(dm,df,cl)\tex1\tex2\tex3\tex5\tex6"	>> "f(diff).txt"		
cat "f(diff)_IT.txt" >> "f(diff).txt"
rm -f "f(diff)_IT.txt"  

echo -e "\nTIME:" >> "f(diff).txt"
cat "f(diff)_TIME.txt" >> "f(diff).txt"
sed -E -i 's: :\t:g' "f(diff).txt"

rm -f "f(diff)_TIME.txt"

########################################################################
echo "On fixe diff et nbcl et on varie >dim<"
########################################################################
diff=40
nbcl=5
liste_dim="10 20 40 60"

[ -f "f(dim)_tmp.txt" ] &&  rm -f "f(dim)_tmp.txt"
for dim in $liste_dim; do
	echo -ne "($dim,$diff,$nbcl)"
	for exo in $liste_exo; do
		result_it=0
		result_temps=0
		# test 10 times
		for i in {1..10}; do
			result=$(../Main_Flood-It ${dim} ${nbcl} ${diff} ${graine} ${exo} ${aff})
			result_it=$(($result_it + $(echo $result | cut -d' ' -f1)))
			result_temps=$(bc -l <<< "scale=6; $result_temps + $(echo $result | cut -d' ' -f2) ")
		done
		result_it=$(bc -l <<< "scale=0; ${result_it}/10")
		result_temps=$(bc -l <<< "scale=6; ${result_temps}/10")
		echo -ne "\t${result_it}\t${result_temps}"
	done
	echo ""
done >> "f(dim)_tmp.txt"

[ -f "f(dim)_TIME.txt" ] &&  rm -f "f(dim)_TIME.txt"
[ -f "f(dim)_IT.txt" ] &&  rm -f "f(dim)_IT.txt"
while read line; do
	echo  $line | cut -d' ' -f'1,2,4,6,8,10,12' >> "f(dim)_IT.txt"
	echo  $line | cut -d' ' -f'1,3,5,7,9,11,13' >> "f(dim)_TIME.txt"
done < "f(dim)_tmp.txt" 
rm -f "f(dim)_tmp.txt"

[ -f "f(dim).txt" ] &&  rm -f "f(dim).txt"
echo -e "f(dim)\nbcl=$nbcl\ndiff=$diff\ngraine=$graine\n" >>"f(dim).txt"
echo "ITERATIONS:" >> "f(dim).txt"
echo -e "(dm,df,cl)\tex1\tex2\tex3\tex5\tex6"	>> "f(dim).txt"	
cat "f(dim)_IT.txt" >> "f(dim).txt"
rm -f "f(dim)_IT.txt"  

echo -e "\nTIME:" >> "f(dim).txt"
cat "f(dim)_TIME.txt" >> "f(dim).txt"
sed -E -i 's: :\t:g' "f(dim).txt"

rm -f "f(dim)_TIME.txt"
