#include "Exo5.h"

Bordure_graphe *creer_bordure(int nbcl, Graphe_zone *pg)
{
	Bordure_graphe *pb = (Bordure_graphe *)malloc(sizeof(Bordure_graphe));
	pb->nbcl = nbcl;
	pb->liste_tailles = (int *)malloc(sizeof(int) * nbcl);
	pb->liste_sommets = (Cellule_som **)malloc(sizeof(Cellule_som *) * nbcl);

	for (int i = 0; i < nbcl; i++) {
		pb->liste_tailles[i] = 0;
		pb->liste_sommets[i] = NULL;
	}

	/* initialize starting point */
	Sommet *ps = pg->mat[0][0];
	ps->marque = 1;
	pb->liste_tailles[ps->cl] = ps->nbcase_som;
	ajoute_liste_sommet(ps, &(pb->liste_sommets[ps->cl]));

	return pb;
};

int get_max_color(Bordure_graphe *pb) {
	int color = 0;
	for (int i = 0; i < pb->nbcl; i++) {
		if (pb->liste_tailles[i] > pb->liste_tailles[color]) {
			color = i;
		}
	};
	return color;
}

int maj_bordure(Bordure_graphe *pb, ListeCase *zsg, int color) 
{
	/* take in bordering sommets of that color */
	Cellule_som *take_in = pb->liste_sommets[color];
	for (Cellule_som *pc = take_in; pc != NULL; pc = pc->suiv) {
		
		/* put sommets of that color into Zsg */
		for (Elnt_liste *pe = pc->sommet->cases; pe != NULL; pe = pe->suiv) {
			ajoute_en_tete(zsg, pe->i, pe->j);
		}
		/* mark sommet as in zsg */
		pc->sommet->marque = 0;


		/* put these sommets' adjacent sommets in Bordure (those who are not already in Bordure nor Zsg) */
		for (Cellule_som *pca = pc->sommet->sommet_adj; pca != NULL; pca = pca->suiv) {

			Sommet *psa = pca->sommet;

			/* is in Zsg already */
			if (psa->marque == 0) continue;

			/* is in Bordure already */
			if (psa->marque == 1) continue;

			/* is not visited yet */
			// if (pca->marque == 2)
			/* put in Bordure */
			psa->marque = 1;
			pb->liste_tailles[psa->cl] += psa->nbcase_som;
			ajoute_liste_sommet(psa, &(pb->liste_sommets[psa->cl]));
		}
	}

	/* 
	 *	Remove taken-in sommets from Bordure
	 *	Note that this will not affect newly added sommets
	 *	because they are of different colors
	 *	Note that free() leaves garbage value - must re-initialize with NULL
	 */
	pb->liste_tailles[color] = 0;
	detruit_liste_sommet(pb->liste_sommets[color]);
	pb->liste_sommets[color] = NULL;

	return color;
};

void libere_bordure(Bordure_graphe *pb)
{
	free(pb->liste_tailles);
	for (int i = 0; i < pb->nbcl; i++) {
		detruit_liste_sommet(pb->liste_sommets[i]);
	}
	free(pb);
};

int strategie_max_bordure(int **M, Grille *G, int dim, int nbcl, int aff)
{
	int count = 0;
	
	/* create zsg for display */
	ListeCase zsg = NULL;
	init_liste(&zsg);

	Graphe_zone *pg = cree_graphe_zone(M, dim);
	Bordure_graphe *pb = creer_bordure(nbcl, pg);
	while (1) {
		/* stop if no remaining border tile */
		int sum = 0;
		for (int i = 0; i < nbcl; i++) {
			sum += pb->liste_tailles[i];
		}
		if (sum == 0) break;

		/* MAJ bordure */
		int color = maj_bordure(pb, &zsg, get_max_color(pb));
		count++;

		// display
		if (aff == 1) {
			// update color of Grille G and redraw
			for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
				Grille_attribue_couleur_case(G,pe->i,pe->j,color);
			}
			// note: if no display needed, Grille G wont be updated like matrix M
			Grille_redessine_Grille(G);
			Grille_attente_touche();
		}
	}

	detruit_liste(&zsg);
	libere_graphe_zone(pg, dim);
	libere_bordure(pb);
	
	return count;
};