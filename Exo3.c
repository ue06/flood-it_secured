#include "Exo3.h"

/*
 *	Usage
 *	S_Zsg *my_zone;
 *	init_Zsg(&myzone, 10, 5);
 */
void init_Zsg(S_Zsg **zsg, int dim, int nbcl) {

	S_Zsg *z = (S_Zsg *)malloc(sizeof(S_Zsg));

	z->dim = dim;
	z->nbcl = nbcl;

	// Lzsg vide
	z->Lzsg = NULL;

	// une liste pour chaque couleur
	z->B = (ListeCase *)malloc(sizeof(ListeCase) * nbcl);
	for (int i = 0; i < nbcl; i++)
		z->B[i] = NULL;

	z->App = (int **)malloc(sizeof(int *) * dim);
	for (int i = 0; i < dim; i++)
		z->App[i] = (int *)malloc(sizeof(int) * dim);

	*zsg = z;
}


void ajoute_Zsg(int i, int j, S_Zsg *zsg) {
	// ajouter dans Lzsg
	Elnt_liste *e = (Elnt_liste *)malloc(sizeof(Elnt_liste));
	e->i = i;
	e->j = j;
	e->suiv = zsg->Lzsg;
	zsg->Lzsg = e;
	// mettre a jour int **App
	zsg->App[i][j] = -1;
}

void ajoute_Bordure(int i, int j, int cl, S_Zsg *zsg) {
	// ajouter dans B
	Elnt_liste *e = (Elnt_liste *)malloc(sizeof(Elnt_liste));
	e->i = i;
	e->j = j;
	e->suiv = zsg->B[cl];
	zsg->B[cl] = e;
	// mettre a jour int **App
	zsg->App[i][j] = cl;
}

int appartient_Zsg(int i, int j, S_Zsg zsg) {
	return (zsg.App[i][j] == -1);
}

int appartient_Bordure(int i, int j, int cl, S_Zsg zsg) {
	return (zsg.App[i][j] == cl);
}

int agrandit_zone(int **M, S_Zsg *Z, int cl, int k, int l) {
	int increase = 0;

	// zone contenant (k, l) de coueur cl
	ListeCase todo = NULL;
	init_liste(&todo);
	ajoute_en_tete(&todo, k, l);

	// traverse todo
	while (test_liste_vide(&todo) == 0) {

		// depile
		int ek, el;
		enleve_en_tete(&todo, &ek, &el);

		// check color
		int e_color = M[ek][el];

		// if same
		if (e_color == cl) {

			// add to Lzsg if not already in there
			int found = 0;
			for (Elnt_liste *pe = Z->Lzsg; pe != NULL; pe = pe->suiv) {
				if (pe->i == ek && pe->j == el) found = 1;
			}
			if (found != 1) {
				ajoute_en_tete(&Z->Lzsg, ek, el);
				increase++;

				// empile neighbors
				if (ek != Z->dim-1)	ajoute_en_tete(&todo, ek+1, el);
				if (ek != 0)		ajoute_en_tete(&todo, ek-1, el);
				if (el != Z->dim-1)	ajoute_en_tete(&todo, ek, el+1);
				if (el != 0)		ajoute_en_tete(&todo, ek, el-1);
			}
		}
		// if different
		else {

			// add to B[e_color] if not already in there
			int found = 0;
			for (Elnt_liste *pe = Z->B[e_color]; pe != NULL; pe = pe->suiv) {
				if (pe->i == ek && pe->j == el) found = 1;
			}
			if (found != 1) {
				ajoute_en_tete(&Z->B[e_color], ek, el);
			}
		}
	}

	return increase;
}

int sequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff) {
	int cpt = 0;

	// initialize zsg
	S_Zsg *zsg = NULL;
	init_Zsg(&zsg, dim, nbcl);
	int taille = 0;
	taille += agrandit_zone(M, zsg, M[0][0], 0, 0);

	// game starts
	while (taille != dim * dim) {
		cpt++;

		// get random color
		int new_color = rand() % nbcl;

		// modify color in zsg
		for (Elnt_liste *pe = zsg->Lzsg; pe != NULL; pe = pe->suiv) {
			M[pe->i][pe->j] = new_color;
		}

		// agrandit_zone pour chaque case de B[new_color]
		for (Elnt_liste *pe = zsg->B[new_color]; pe != NULL; pe = pe->suiv) {
			taille += agrandit_zone(M, zsg, new_color, pe->i, pe->j);
		}

		// detruire B[new_color]
		detruit_liste(&zsg->B[new_color]);

		// display
		if (aff == 1) {
			// update color of Grille G and redraw
			for (Elnt_liste *pe = zsg->Lzsg; pe != NULL; pe = pe->suiv) {
				Grille_attribue_couleur_case(G,pe->i,pe->j,M[pe->i][pe->j]);
			}
			// note: if no display needed, Grille G wont be updated like matrix M
			Grille_redessine_Grille(G);
			Grille_attente_touche();
		}
	}

	return cpt;
}