#include<stdio.h>
#include "Exo2.h"

void trouve_zone_imp(int **M, int nbcase, int i, int j, int *taille, ListeCase *L){
	int src_color = M[i][j];

	// create stack
	ListeCase todo;
	init_liste(&todo);
	ajoute_en_tete(&todo, i, j);

	// traverse todo
	while (test_liste_vide(&todo) == 0) {
		
		// depile
		int ei, ej;
		enleve_en_tete(&todo, &ei, &ej);
		
		// if same color
		int e_color = M[ei][ej];
		if (e_color == src_color) {

			// and not already in todo nor in L
			int checked = 0;
			// not in L
			for (Elnt_liste *pe = *L; pe != NULL; pe = pe->suiv) {
				if (pe->i == ei && pe->j == ej) checked = 1;
			}
			// no other appearance in todo
			for (Elnt_liste *pe = todo; pe != NULL; pe = pe->suiv) {
				if (pe->i == ei && pe->j == ej) checked = 1;
			}

			if (checked != 1) {
				// then add to L
				ajoute_en_tete(L, ei, ej);
				*taille = *taille + 1;

				// and empile neighbors
				if (ei != nbcase-1)	ajoute_en_tete(&todo, ei+1, ej);
				if (ei != 0)		ajoute_en_tete(&todo, ei-1, ej);
				if (ej != nbcase-1)	ajoute_en_tete(&todo, ei, ej+1);
				if (ej != 0)		ajoute_en_tete(&todo, ei, ej-1);
			}
		}
	}
}

int sequence_aleatoire_imp(int **M, Grille *G, int dim, int nbcl, int aff){
  int cpt = 0;
  int new_color;
  // get random color
  do{
  	new_color = rand() % nbcl;
  } while(new_color == M[0][0]);

  // find zsg
  ListeCase zsg;
  int taille = 0;
  init_liste(&zsg);
  trouve_zone_imp(M, dim, 0, 0, &taille, &zsg);

  // have i won?
  if (taille == dim * dim) return 0;

  // modify color in zsg
  for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
  	M[pe->i][pe->j] = new_color;
  }

  // display
  if (aff == 1) {
  	// update color of Grille G and redraw
  	for (Elnt_liste *pe = zsg; pe != NULL; pe = pe->suiv) {
  		Grille_attribue_couleur_case(G,pe->i,pe->j,M[pe->i][pe->j]);
	}
	// note: if no display needed, Grille G wont be updated like matrix M
	Grille_redessine_Grille(G);
	Grille_attente_touche();
  }

  // destroy list zsg
  detruit_liste(&zsg);

  // recursive call
  cpt = 1 + sequence_aleatoire_imp(M, G, dim, nbcl, aff);

  return cpt;
}