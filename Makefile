CC=gcc -ggdb

all: Main_Flood-It

API_Gene_instance.o: API_Gene_instance.c API_Gene_instance.h
	$(CC) -c API_Gene_instance.c `sdl2-config --cflags`

API_Grille.o: API_Grille.c API_Grille.h
	$(CC) -c API_Grille.c `sdl2-config --cflags`

Liste_case.o: Liste_case.c Liste_case.h
	$(CC) -c Liste_case.c `sdl2-config --cflags`

queue.o: queue.c queue.h
	$(CC) -c queue.c `sdl2-config --cflags`

Exo1.o: Exo1.c Exo1.h Liste_case.h
	$(CC) -c Exo1.c `sdl2-config --cflags`

Exo2.o: Exo2.c Exo2.h Liste_case.h
	$(CC) -c Exo2.c `sdl2-config --cflags`

Exo3.o: Exo3.c Exo3.h Liste_case.h
	$(CC) -c Exo3.c `sdl2-config --cflags`

Exo4.o: Exo4.c Exo4.h Liste_case.h
	$(CC) -c Exo4.c `sdl2-config --cflags`

Exo5.o: Exo5.c Exo5.h Liste_case.h
	$(CC) -c Exo5.c `sdl2-config --cflags`

Exo6.o: Exo6.c Exo6.h Liste_case.h queue.h
	$(CC) -c Exo6.c `sdl2-config --cflags`

Main_Flood-It.o: Main_Flood-It.c
	$(CC) -c Main_Flood-It.c `sdl2-config --cflags`

Main_Flood-It: Main_Flood-It.o Liste_case.o queue.o API_Grille.o API_Gene_instance.o Exo1.o Exo2.o Exo3.o Exo4.o Exo5.o Exo6.o
	$(CC) -o Main_Flood-It Main_Flood-It.o Liste_case.o queue.o API_Grille.o API_Gene_instance.o Exo1.o Exo2.o Exo3.o Exo4.o Exo5.o Exo6.o `sdl2-config --libs` 

clean:
	rm -f *.o Main_Flood-It
