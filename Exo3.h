#ifndef __EXO3__
#define __EXO3__

#include "API_Grille.h"
#include "Liste_case.h"

typedef struct {
	int dim;
	int nbcl;

	ListeCase Lzsg;
	ListeCase *B;
	int **App;
} S_Zsg;

// initialise la structure
void init_Zsg(S_Zsg **zsg, int dim, int nbcl);

// ajoute une case dans la liste Lzsg
void ajoute_Zsg(int i, int j, S_Zsg *zsg);

// ajoute une case dans la bordure d’une couleur cl donnée
void ajoute_Bordure(int i, int j, int cl, S_Zsg *zsg);

// renvoie vrai si une case est dans LZsg
int appartient_Zsg(int i, int j, S_Zsg zsg);

// renvoie vrai si une case est dans la bordure de couleur cl donnée
int appartient_Bordure(int i, int j, int cl, S_Zsg zsg);

int agrandit_zone(int **M, S_Zsg *Z, int cl, int k, int l);

int sequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff);
#endif /* __EXO3__ */