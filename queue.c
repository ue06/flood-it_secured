#include<stdlib.h>
#include "queue.h"

Queue *creer_file()
{
	Queue *pq = (Queue *)malloc(sizeof(Queue));
	pq->start = NULL;
	pq->end = NULL;
	pq->length = 0;
	return pq;
};

int empile(Queue *pq, Sommet *ps)
{
	/* create Elnt_sommet */
	Elnt_sommet *pe = (Elnt_sommet *)malloc(sizeof(Elnt_sommet));
	if (pe == NULL) return 0;

	pe->sommet = ps;
	pe->prev = NULL;
	pe->next = pq->start;

	/* empile */
	if (pq->length != 0) pq->start->prev = pe;

	pq->start = pe;
	pq->length += 1;

	if (pq->length == 1) pq->end = pe;

	/* ok */
	return 1;
};

int test_file_vide(Queue *pq)
{
	if (pq->length == 0) return 1;
	else return 0;
};

Sommet *depile(Queue *pq)
{
	if (test_file_vide(pq)) return NULL;

	/* get last element's sommet */
	Elnt_sommet *end = pq->end;
	Sommet *ps = end->sommet;

	/* queue management*/
	pq->end = end->prev;
	pq->length -= 1;
	if (pq->length != 0) pq->end->next = NULL;
	if (pq->length == 0) pq->start = NULL;

	/* free memory for last element without touching its sommet */
	free(end);

	return ps;
};

void detruit_file(Queue *pq)
{
	while (test_file_vide(pq) == 0) {
		depile(pq);
	}
	free(pq);
};